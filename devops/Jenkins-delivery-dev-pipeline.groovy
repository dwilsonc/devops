node {
    def mvnHome
    
    stage('Preparacionv1') {
        try {
            git 'https://dwilsonc@bitbucket.org/dwilsonc/devops.git'
            mvnHome = tool 'M2'
        } catch (e) {
            notifyStarted("Error al clonar de gitlab!")
            throw e
        } 
    }
    
    stage('Prueba') {
        try {
            sh "'${mvnHome}/bin/mvn' test"
        } catch (e) {
            notifyStarted("Tests Failed in Jenkins!")
            throw e
        } 
    }
    stage('Construccion') {
        try {
            sh "'${mvnHome}/bin/mvn' clean package -DskipTests"
        }catch (e) {
            notifyStarted("Build Failed in Jenkins!")
            throw e
        } 
    }
    
    stage('Resultados') {
        try{
            archive 'target/*.jar'
        }catch (e) {
            notifyStarted("Packaging Failed in Jenkins!")
            throw e
        } 
    }

    stage('Despliegue') {
        try{
            sh '/var/lib/jenkins/workspace/Devops/runDeployment.sh'
        }catch (e) {
            notifyStarted("Deployment Failed in Jenkins!")
            throw e
        } 
    }
    
    notifyStarted("Tu codigo ha sido testado y desplegado con Exito!!.")
}

def notifyStarted(String message) {
  slackSend (color: '#FFFF00', message: "${message}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
}